#!/bin/bash

apt --yes --force-yes update
apt --yes --force-yes full-upgrade
apt --yes --force-yes install vim
apt --yes --force-yes install git

useradd -u 1919 -m drizzutojr
echo -e "drizzutojr\ndrizzutojr" | passwd drizzutojr
passwd -e drizzutojr
mkdir /home/drizzutojr/.ssh
ssh-keygen -t rsa -b 4096 -C "danno@drizzutojr.com" -f /home/drizzutojr/.ssh/id_rsa -q -N ""

cat bashrc >> /home/drizzutojr/.bashrc
cp vimrc /home/drizzutojr/vimrc

echo "# Allow drizzutojr to run bash as sudo"
echo 'drizzutojr ALL=(ALL) /bin/bash, /usr/bin/bash' | sudo EDITOR='tee -a' visudo

echo "AllowUsers drizzutojr" >> /etc/ssh/sshd_config
systemctl restart sshd

chown -R drizzutojr:drizzutojr /home/drizzutojr